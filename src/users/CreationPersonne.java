package users;

/**
 * Importations
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import bdd.Etablissement;
import bdd.Matiere;
import bdd.Matieres;
import bulletin.Bulletin;
import emploiDuTemps.Promotion;
import main.Main;

/**
 * Classe CreationPersonne
 * @author Bouillet Ducatillion
 *
 */
public class CreationPersonne {

	/**
	 * Attributs
	 */
	private String nomUtilisateur;
	private String motDePasse;

	/**
	 * Constructeur de CreationPersonne
	 * @param nomUtilisateur
	 * @param motDePasse
	 */
	public CreationPersonne(String nomUtilisateur, String motDePasse) {
		this.motDePasse = motDePasse;
		this.nomUtilisateur = nomUtilisateur;
	}

	/**
	 * Constructeur de CreationPersonne sans parametre
	 */
	public CreationPersonne() {
	}

	/**
	 * Methode creationPersonnelAdministratif permettant de creer un PersonnelAdministratif a partir de la BDD
	 * @return le personnelAdministratif cree
	 */
	public PersonnelAdministratif creationPersonnelAdministratif() {
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM identifiants WHERE login = ? AND password = ?");
			statement.setString(1,nomUtilisateur);
			statement.setString(2,motDePasse);
			ResultSet resultat = statement.executeQuery();
			if (resultat.next()) {
				int id = resultat.getInt(2);
				PreparedStatement statement2 = Main.getConnection().prepareStatement("SELECT * FROM personneladministratif WHERE id = ?");
				statement2.setInt(1,id);
				ResultSet resultat2 = statement2.executeQuery();
				if (resultat2.next()) {
					String nom = resultat2.getString(2);
					nom = nom.trim();
					String prenom = resultat2.getString(3);
					prenom = prenom.trim();
					String adresse = resultat2.getString(4);
					adresse = adresse.trim();
					String fonction = resultat2.getString(5);
					fonction = fonction.trim();
					Date dateNaissance = resultat2.getDate(6);
					PersonnelAdministratif personnelAdministratif = new PersonnelAdministratif(nomUtilisateur, motDePasse, nom, prenom, fonction, adresse, dateNaissance);
					return personnelAdministratif;
				}
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Methode permettant de creer un personnel administratif a partir de son nom, prenom et la BDD
	 * @param etablissement
	 * @return
	 */
	public PersonnelAdministratif creationPersonnelAdministratifNP(Etablissement etablissement) {
		for(int i =0; i<etablissement.getPersonnelAdministratif().size(); i++) {
			System.out.println(etablissement.getPersonnelAdministratif().get(i).getNom() + " " + etablissement.getPersonnelAdministratif().get(i).getPrenom());
		}
		System.out.println("Veuillez saisir le nom du personnel administratif");
		String nom = Main.getScanner().nextLine();
		nom = nom.trim();
		System.out.println("Veuillez saisir le prenom du personnel administratif");
		String prenom = Main.getScanner().nextLine();
		prenom = prenom.trim();
		PersonnelAdministratif personnelAdministratif = null;
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM personneladministratif WHERE prenom = ? AND nom = ?");
			statement.setString(1, prenom);
			statement.setString(2, nom);
			ResultSet resultat = statement.executeQuery();
			if (resultat.next()) {
				String adresse = resultat.getString(4);
				adresse = adresse.trim();
				String fonction = resultat.getString(5);
				fonction = fonction.trim();
				Date dateNaissance = resultat.getDate(6);
				PersonnelAdministratif persAdmin = new PersonnelAdministratif(nom, prenom, fonction, adresse, dateNaissance);
				personnelAdministratif = persAdmin;
					}
		}
		catch (SQLException e) {
			System.out.println("Nom et/ou prenom sont mal orthographiés");
			e.printStackTrace();
		}
		return personnelAdministratif;
	}

	/**
	 * Methode creationProfesseur permettant de creer un professeur grace a la BDD
	 * @return le professeur cree
	 */
	public Professeur creationProfesseur() {
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM identifiants WHERE login = ? AND password = ?");
			statement.setString(1,nomUtilisateur);
			statement.setString(2,motDePasse);
			ResultSet resultat = statement.executeQuery();
			if (resultat.next()) {
				int id = resultat.getInt(2);
				PreparedStatement statement2 = Main.getConnection().prepareStatement("SELECT * FROM professeurs WHERE id = ?");
				statement2.setInt(1,id);
				ResultSet resultat2 = statement2.executeQuery();
				if (resultat2.next()) {
					String nom = resultat2.getString(2);
					nom = nom.trim();
					String prenom = resultat2.getString(3);
					prenom = prenom.trim();
					String adresse = resultat2.getString(4);
					adresse = adresse.trim();
					String matiereString = resultat2.getString(5);
					matiereString = matiereString.trim();
					Matiere matiere = Matieres.get(matiereString);
					Date dateNaissance = resultat2.getDate(6);
					Professeur professeur = new Professeur(nomUtilisateur, motDePasse, nom, prenom, matiere, adresse, dateNaissance);
					return professeur;
				}
			}

		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Methode permettant de creer un professeur a partir de son nom, prenom et la BDD
	 * @param etablissement
	 * @return
	 */
	public Professeur creationProfesseurNP(Etablissement etablissement) {
		for(int i = 0; i<etablissement.getProfesseurs().size(); i++) {
			System.out.println(etablissement.getProfesseurs().get(i).getNom() + " " + etablissement.getProfesseurs().get(i).getPrenom());
		}
		System.out.println("Veuillez saisir le nom du professeur");
		String nom = Main.getScanner().nextLine();
		nom = nom.trim();
		System.out.println("Veuillez saisir le prenom du professeur");
		String prenom = Main.getScanner().nextLine();
		prenom = prenom.trim();
		Professeur professeur = null;
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM professeurs WHERE prenom = ? AND nom = ?");
			statement.setString(1, prenom);
			statement.setString(2, nom);
			ResultSet resultat = statement.executeQuery();
			if (resultat.next()) {
				String adresse = resultat.getString(4);
				adresse = adresse.trim();
				String matiereString = resultat.getString(5);
				matiereString = matiereString.trim();
				Matiere matiere = Matieres.get(matiereString); 
				Date dateNaissance = resultat.getDate(6);
				Professeur prof = new Professeur(nom, prenom, matiere, adresse, dateNaissance);
				professeur = prof;
			}
		}
		catch (SQLException e) {
			System.out.println("Nom et/ou prenom sont la orthographies");
			e.printStackTrace();
		}
		return professeur;
	}

	/**
	 * Methode creationEtudiant permettant de creer un etudiant grace a la BDD
	 * @param etablissement
	 * @return l'etudiant cree
	 */
	public Etudiant creationEtudiant(Etablissement etablissement) {
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM identifiants WHERE login = ? AND password = ?");
			statement.setString(1,nomUtilisateur);
			statement.setString(2,motDePasse);
			ResultSet resultat = statement.executeQuery();
			if (resultat.next()) {
				int id = resultat.getInt(2);
				PreparedStatement statement2 = Main.getConnection().prepareStatement("SELECT * FROM eleves WHERE id = ?");
				statement2.setInt(1,id);
				ResultSet resultat2 = statement2.executeQuery();
				if (resultat2.next()) {
					String nom = resultat2.getString(3);
					nom = nom.trim();
					String prenom = resultat2.getString(2);
					prenom = prenom.trim();
					String adresse = resultat2.getString(5);
					adresse = adresse.trim();
					String promotionString = resultat2.getString(4);
					promotionString = promotionString.trim();
					Promotion promotion = Promotion.get(promotionString, etablissement); 
					Date dateNaissance = resultat2.getDate(6);
					Bulletin bulletin = new Bulletin(nom, prenom);
					Etudiant etudiant = new Etudiant(nomUtilisateur, motDePasse, nom, prenom, promotionString, promotion, adresse, dateNaissance, bulletin);
					return etudiant;
				}
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Methode permettant de creer un etudiant a partir de son nom, prenom et la BDD
	 * @param etablissement
	 * @return un etudiant
	 */
	public Etudiant creationEtudiantNP(Etablissement etablissement) {
		for (int i = 0; i<etablissement.getEtudiants().size(); i++) {
			System.out.println(etablissement.getEtudiants().get(i).getNom() + " " + etablissement.getEtudiants().get(i).getPrenom());
		}
		System.out.println("Veuillez saisir le nom de l'etudiant");
		String nom = Main.getScanner().nextLine();
		nom = nom.trim();
		System.out.println("Veuillez saisir le prenom de l'etudiant");
		String prenom = Main.getScanner().nextLine();
		prenom = prenom.trim();
		Etudiant etudiant = null;
		Bulletin bulletin = new Bulletin(nom, prenom);
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM eleves WHERE prenom = ? AND nom = ?");
			statement.setString(1, prenom);
			statement.setString(2, nom);
			ResultSet resultat = statement.executeQuery();
			if (resultat.next()) {
				String adresse = resultat.getString(5);
				adresse = adresse.trim();
				String promotionString = resultat.getString(4);
				promotionString = promotionString.trim();
				Promotion promotion = Promotion.get(promotionString, etablissement); 
				Date dateNaissance = resultat.getDate(6);
				Etudiant etu = new Etudiant(nom, prenom, promotion, adresse, dateNaissance, bulletin);
				etudiant = etu;
			}
		}
		catch (SQLException e) {
			System.out.println("Nom et/ou prenom sont mal orthographies");
			e.printStackTrace();
		}
		return etudiant;
	}
}
