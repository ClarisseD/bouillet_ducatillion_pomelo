package users;

/**
 * Importations
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import bdd.Etablissement;
import bdd.Matiere;
import emploiDuTemps.Cours;
import emploiDuTemps.EmploiDuTemps;
import fiche.FicheInformation;
import main.Main;

/**
 * Classe Professeur qui herite de Utilisateur
 * @author Bouillet Ducatillion
 *
 */
public class Professeur extends Utilisateur {

	/**
	 * Attributs
	 */
	private Matiere matiere;
	private EmploiDuTemps edt;

	/**
	 * Getter de matiere
	 * @return
	 */
	public Matiere getMatiere() {
		return matiere;
	}

	/**
	 * Getter et setter edt
	 * @return
	 */
	public EmploiDuTemps getEdt() {
		return edt;
	}

	/**
	 * Getter du type correspond a un professeur
	 */
	public int getType(){
		return 1;
	}

	/**
	 * Constructeur Professeur
	 * @param nom
	 * @param prenom
	 * @param matiere
	 * @param adresse
	 * @param dateNaissance
	 * @param nomUtilisateur
	 * @param motDePasse
	 */
	public Professeur(String nomUtilisateur, String motDePasse, String nom, String prenom, Matiere matiere, String adresse, 
			Date dateNaissance) {
		super(nomUtilisateur, motDePasse, nom, prenom, adresse, dateNaissance);
		this.matiere = matiere;
	}

	/**
	 * Constructeur Professeur
	 * @param nom
	 * @param prenom
	 * @param matiere
	 * @param adresse
	 * @param dateNaissance
	 */
	public Professeur(String nom, String prenom, Matiere matiere, String adresse, 
			Date dateNaissance) {
		super(nom, prenom, adresse, dateNaissance);
		this.matiere = matiere;
	}

	/**
	 * Methode visualiserFiche permettant de visualiser sa fiche personnelle en tant que professeur 
	 * @param professeur concerne par la fiche
	 * @return String, Liste des attributs de la personne 
	 */
	public String visualiserFiche(Professeur professeur) {
		FicheInformation fI = new FicheInformation(professeur);
		return fI.toString();
	}

	/**
	 * Methode saisirNote permet d'ajouter une note au bulletin de l'etudiant concerne
	 * @param valeur
	 * @param coeff
	 * @param nomEtudiant
	 * @return une note a ajouter au bulletin d'un etudiant
	 */
	public void saisirNote(Etablissement eta) {

		System.out.println("Veuillez saisir la valeur de la note : ");
		float val = Main.getScanner().nextFloat();
		System.out.println("Veuillez saisir le coefficient de la note : ");
		int coeff = Main.getScanner().nextInt();
		Main.getScanner().nextLine();
		CreationPersonne etu = new CreationPersonne();
		Etudiant etudiant = etu.creationEtudiantNP(eta);

		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("INSERT INTO notes VALUES (?, ?, ?, ?, ?)");
			statement.setString(1, etudiant.getNom());
			statement.setString(2, etudiant.getPrenom());
			statement.setInt(3, coeff);
			statement.setString(4, "" + this.matiere + "");
			statement.setFloat(5, val);
			statement.execute();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode saisirAppreciation permet d'ecrire une appreciation pour l'etudiant concerne
	 * @return appreciation a ajoute au bulletin de l'etudiant concerne
	 */
	public void saisirAppreciation(Etablissement eta) {

		CreationPersonne etu = new CreationPersonne();
		Etudiant etudiant = etu.creationEtudiantNP(eta);
		System.out.println("veuillez saisir le contenu de l'appreciation : ");
		String appreciation = Main.getScanner().nextLine();
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM appreciation WHERE nom = ? AND prenom = ?");
			statement.setString(1, etudiant.getNom());
			statement.setString(2, etudiant.getPrenom());
			ResultSet resultat = statement.executeQuery();
			if(resultat.next()) {
				PreparedStatement statement2 = Main.getConnection().prepareStatement("DELETE FROM appreciation WHERE nom = ? AND prenom = ?");
				statement2.setString(1, etudiant.getNom());
				statement2.setString(2, etudiant.getPrenom());
				statement2.execute();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("INSERT INTO appreciation VALUES (?, ?, ?, ?)");
			statement.setString(1, etudiant.getNom());
			statement.setString(2, etudiant.getPrenom());
			statement.setString(3, "" + this.matiere + "");
			statement.setString(4, appreciation);
			statement.execute();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode visualiserEdT permettant de visualiser l'emploi du temps du professeur
	 * @param edt
	 */
	public void visualiserEdT(EmploiDuTemps edt){
		int tailleList = edt.getEdt().size();
		for (int i=0; i<tailleList ; i++ ) {
			Cours cours = edt.getEdt().get(i);
			System.out.println(i + informationsCours(cours));
		}
	}

	/**
	 * Methode informationsCours permettant de visualiser les informations d'un cours
	 * @param cours
	 * @return les informations d'un cours
	 */
	public String informationsCours(Cours cours) {
		return cours.toString();
	}
}
