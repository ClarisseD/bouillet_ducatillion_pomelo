package users;

/**
 * Importations
 */
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bulletin.Appreciation;
import bulletin.Bulletin;
import bulletin.Note;
import emploiDuTemps.Cours;
import emploiDuTemps.EmploiDuTemps;
import emploiDuTemps.Promotion;
import fiche.FicheInformation;

/**
 * Classe Etudiant qui herite de Utilisateur
 * @author Bouillet Ducatillion
 *
 */
public class Etudiant extends Utilisateur{
	
	/**
	 * Attributs
	 */
	private String promotionString;
	private Promotion promotion;
	private Bulletin bulletin;
	
	/**
	 * Getter et setter de promotionString
	 * @return
	 */
	public String getPromotionString() {
		return promotionString;
	}
	public void setPromotionString(String promotionString) {
		this.promotionString = promotionString;
	}
	
	/**
	 * Getter et setter promotion
	 * @return
	 */
	public Promotion getPromotion() {
		return promotion;
	}
	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}
	
	/**
	 * Getter et setter bulletin
	 * @return
	 */
	public Bulletin getBulletin() {
		return bulletin;
	}
	public void setBulletin(Bulletin bulletin) {
		this.bulletin = bulletin;
	}
	
	/**
	 * Getter du type correspondant a un etudiant
	 */
	public int getType(){
		return 2;
	}
 	
 	/**
	 * Constructeur avec tous les attributs de Etudiant
	 * @param nom
	 * @param prenom
	 * @param promotion
	 * @param adresse
	 * @param dateNaissance
	 */
 	public Etudiant(String nom, String prenom, Promotion promotion, 
			String adresse, Date dateNaissance, Bulletin bulletin) {
		super(nom, prenom, adresse, dateNaissance);
		this.promotion = promotion;
		this.bulletin = bulletin;
	}
 	
 	/**
	 * Constructeur avec tous les attributs de Etudiant
	 * @param nom
	 * @param prenom
	 * @param promotion
	 * @param adresse
	 * @param dateNaissance
	 * @param nomUtilisateur
	 * @param motDePasse
	 */
 	public Etudiant(String nomUtilisateur, String motDePasse, String nom, String prenom, String promotionString, Promotion promotion, 
			String adresse, Date dateNaissance, Bulletin bulletin) {
		super(nomUtilisateur, motDePasse, nom, prenom, adresse, dateNaissance);
		this.promotion = promotion;
		this.setPromotionString(promotionString);
		this.bulletin = bulletin;
	}	
	
	/**
	 * Methode visualiserFiche permettant de visualiser sa fiche personnelle en tant qu'etudiant 
	 * @param etudiant concerne par la fiche
	 * @return String, Liste des attributs de la personne 
	 */
	public String visualiserFiche(Etudiant etudiant) {
		FicheInformation fI = new FicheInformation(etudiant);
		return fI.toString();
	}
	
	/**
	 * Methode visualiserEdT permettant de visualiser l'emploi du temps de la promotion
	 * @param promotion
	 */
	public void visualiserEdT(EmploiDuTemps edt){
		int tailleList = edt.getEdt().size();
		for (int i=0; i<tailleList ; i++ ) {
			Cours cours = edt.getEdt().get(i);
			System.out.println(i + informationsCours(cours));
		}
	}
	
	/**
	 * Methode informationsCours permettant de visualiser les informations d'un cours
	 * @param cours
	 * @return les informations d'un cours
	 */
	public String informationsCours(Cours cours) {
		return cours.toString();
	}
	
	/**
	 * Methode pour visualiser le bulletin
	 */
	public List<String> visualiserBulletin(){
		
		List<Note> moyennes = new ArrayList<Note>();
		List<String> moyenneEtAppreciation = new ArrayList<String>();
		int k = 0;
		List<Integer> index = new ArrayList<Integer>();
		for (List<Note> note : this.bulletin.getNotes()) {
			if(note.size()>1) {
				index.add(k);
			}
			k++;
		}
		if(index.size()==0) {
			System.out.println("Vous n'avez pas de note");
			moyenneEtAppreciation.add("Il n'a pas encore de note");
		}
		else {
			int c = 0;
			int r = 0;
				for(List<Note> notes : this.bulletin.getNotes()) {
					boolean isIn = false;
					for(int d =0; d<index.size(); d++) {
						if(c == index.get(d)) {
							isIn = true;
							break;
						}
					}
					if(isIn) {
						Note moyenne = new Note(1, 0);
						int sommeCoeff = 0;
						System.out.println("Matiere: " + notes.get(0).getMatiere() + "\n" + "Notes: ");
						for(int f =1; f<notes.size(); f++) {
							Note note = notes.get(f);
							System.out.println("" + note.getValeur());
							moyenne = new Note(note.getMatiere(), 1, moyenne.getValeur()+note.getValeur()*note.getCoeff());
							sommeCoeff += note.getCoeff();
						}
						moyenne = new Note(moyenne.getMatiere(), 1, moyenne.getValeur()/sommeCoeff);
						System.out.println("" + "Moyenne: " + moyenne.getValeur());
						moyennes.add(moyenne);
						r++;
						// On considere qu l'etudiant n'a pas d'appreciation s'il n'a pas de note
						boolean thereIs = false;
						for(Appreciation appreciation : this.bulletin.getAppreciations()) {
							if(appreciation.getMatiere() == notes.get(0).getMatiere()) {
								System.out.println("Appreciation: " + appreciation.getContenu());
								thereIs = true;
								moyenneEtAppreciation.add("" + moyenne.getMatiere() + ": " + "\n" + "   Apreciation: " + appreciation.getContenu() + "\n" + "   Moyenne: " + moyenne.getValeur() + "\n" +"\n");
								break;
							}
						}
						if(!thereIs) {
							System.out.println("Vous n'avez pas encore d'appreciation pour cette matiere" + "\n");
							moyenneEtAppreciation.add("" + moyenne.getMatiere() + ": " + "\n" + "   Apreciation: (Pas encore d'appreciation)" + "\n" + "   Moyenne: " + moyenne.getValeur() + "\n"+ "\n");
						}
						else {
							System.out.println("\n");
						}
					}
					if(r == index.size()) {
						break;
					}
					else {
						c++;
					}
				}
			double moyenneGenerale = 0;
			for(Note moyenne : moyennes) {
				moyenneGenerale += moyenne.getValeur();
			}
			moyenneGenerale = moyenneGenerale/moyennes.size();
			System.out.println("Votre moyenne generale actuelle est de: " + moyenneGenerale);
			moyenneEtAppreciation.add("Moyenne generale : " + moyenneGenerale);
		}
		return moyenneEtAppreciation;
	}
}
