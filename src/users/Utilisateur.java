package users;

/**
 * Importations
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bdd.Etablissement;
import communication.Communiquable;
import communication.Message;
import main.Main;

/**
 * Classe Utilisateur qui implemente l'interface Communiquable
 * @author Bouillet Ducatillion
 *
 */
public class Utilisateur implements Communiquable {

	/**
	 * Attributs
	 */
	private String nomUtilisateur;
	private String motDePasse;
	private String nom;
	private String prenom;
	private String adresse;
	private Date dateNaissance;

	/**
	 * Getter de nomUtilisateur 
	 * @return
	 */
	public String getNomUtilisateur() {
		return nomUtilisateur;
	}

	/**
	 * Getter de motDePasse
	 * @return
	 */
	public String getMotDePasse() {
		return motDePasse;
	}

	/**
	 * Getter de nom
	 * @return
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Getter de prenom
	 * @return
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Getter et setter adresse
	 * @return
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * Getter et setter dateNaissance
	 * @return
	 */
	public Date getDateNaissance() {
		return dateNaissance;
	}
	
	/**
	 * Getter du type correspond a un utilisateur
	 * @return
	 */
	public int getType(){
		return 0;
	}

	/**
	 * Constructeur avec tous les attributs de Utilisateur
	 * @param nomUtilisateur
	 * @param motDePasse
	 * @param nom
	 * @param prenom
	 * @param adresse
	 * @param dateNaissance
	 */
	public Utilisateur(String nomUtilisateur, String motDePasse, 
			String nom, String prenom, String adresse, Date dateNaissance) {
		this.nomUtilisateur = nomUtilisateur;
		this.motDePasse = motDePasse;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.dateNaissance = dateNaissance;
	}

	/**
	 * Constructeur avec les attributs nom, prenom, adresse dateNaissance de Utilisateur
	 * @param nom
	 * @param prenom
	 * @param adresse
	 * @param dateNaissance
	 */
	public Utilisateur(String nom, String prenom, String adresse, Date dateNaissance) {
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.dateNaissance = dateNaissance;
	}

	/**
	 * Constructeur avec les 2 attributs de connection de Utilisateur 
	 * @param nomUtilisateur
	 * @param motDePasse
	 */
	public Utilisateur(String nomUtilisateur, String motDePasse) {
		this.nomUtilisateur = nomUtilisateur;
		this.motDePasse = motDePasse;
	}

	/**
	 * Methode connexion permettant de se connecter a son compte 
	 * Il existe 3 types de comptes: Etudiant, Professeur et Personnel Administratif
	 * @throws SQLException 
	 */
	public int connexion() throws SQLException {
		int type = 0;
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM identifiants WHERE login = ? AND password = ?");
			statement.setString(1,nomUtilisateur);
			statement.setString(2,motDePasse);
			ResultSet resultat = statement.executeQuery();
			if(resultat.next()) {
				System.out.println("Connection reussie! Bonjour "+ nomUtilisateur);
				type = resultat.getInt(1);				
			}
			else {
				System.out.println("Nom d'utilisateur et/ou mot de passe incorrect(s)!");
			}
		}
		catch( SQLException e){
			e.printStackTrace();
		}
		return type;
	}

	/**
	 * Surcharge de la methode ecrire
	 * @param other
	 * @return un message
	 */
	@Override
	public Message ecrire(Etablissement etablissement){
		String emetteur = this.nom + " " + this.prenom;
		System.out.println("Voulez-vous ecrire a un : \n -personnel administratif \n -professeur \n -etudiant");
		String desti = Main.getScanner().nextLine();
		Utilisateur destinataire = null;
		if (desti.equals("personnel administratif")) {
			CreationPersonne persAdmin = new CreationPersonne();
			destinataire = persAdmin.creationPersonnelAdministratifNP(etablissement);
		}
		else if (desti.equals("professeur")) {
			CreationPersonne prof = new CreationPersonne();
			destinataire = prof.creationProfesseurNP(etablissement);
		}
		else if (desti.equals("etudiant")) {
			CreationPersonne etu = new CreationPersonne();
			destinataire = etu.creationEtudiantNP(etablissement);
		}
		else {
			System.out.println("ce n'est pas une personne de l'etablissement");
		}
		System.out.println("Veuillez saisir le contenu de votre message (tout ecrire sur une ligne et sans apostrophe)");
		String contenu = Main.getScanner().nextLine();
		Message message = new Message(emetteur, destinataire, contenu);
		return message;
	}
	
	/**
	 * Methode permettant d'ajouter a la BDD le message cree precedemment 
	 * Le destinataire pourra le visualiser quand il se connectera
	 * @param message
	 */
	public void ajoutMessageBDD(Message message){
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("INSERT INTO messages VALUES (?, ?, ?)");
			statement.setString(1, message.getEmetteur());
			statement.setString(2, message.getDestinataire().getNom());
			statement.setString(3, message.getContenu());
			statement.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Surcharge de la methode lire
	 * @param msg
	 * @return le contenu du message
	 */
	@Override
	public String lire(Message msg) {
		return "Voici le message de " + msg.getEmetteur() + ":" + "\n" + msg.getContenu();
	}

	/**
	 * Methode permettant de supprimer un message de la base de donnees
	 * @param message
	 */
	public void supprimerMessageBDD(Message message){
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("DELETE FROM messages WHERE emetteur = ? AND destinataire = ? AND contenu = ?");
			statement.setString(1, message.getEmetteur());
			statement.setString(2, message.getDestinataire().getNom());
			statement.setString(3, message.getContenu());
			statement.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Methode permettant d'afficher la liste de message de l'etudiant
	 * @param messages
	 * @param etudiant
	 * @return
	 */
	public List<Message> afficherListMessage(List<Message> messages, Etudiant etudiant) {
		int i = 0;
		List<Message> messagesEtudiant = new ArrayList<Message>();
		for(Message message : messages) {
			if (message.getDestinataire().getNom().equals(etudiant.getNom())){
				System.out.println("id : " + i + " de : " + message.getEmetteur());
				messagesEtudiant.add(message);
			}
			i++;
		}

		return messagesEtudiant;
	}

	/**
	 * Methode permettant d'afficher la liste de message du professeur
	 * @param messages
	 * @param professeur
	 * @return
	 */
	public List<Message> afficherListMessage(List<Message> messages, Professeur professeur) {
		int i = 0;
		List<Message> messagesProfesseur = new ArrayList<Message>();
		for(Message message : messages) {
			if (message.getDestinataire().getNom().equals(professeur.getNom())){
				System.out.println("id : " + i + " de : " + message.getEmetteur());
				messagesProfesseur.add(message);
			}
			i++;
		}
		return messagesProfesseur;
	}

	/**
	 * Methode permettant d'afficher la liste de message du personnel administratif
	 * @param messages
	 * @param personnelAdministratif
	 * @return
	 */
	public List<Message> afficherListMessage(List<Message> messages, PersonnelAdministratif personnelAdministratif) {
		int i = 0;
		List<Message> messagesPersAdmin = new ArrayList<Message>();
		for(Message message : messages) {
			if (message.getDestinataire().getNom().equals(personnelAdministratif.getNom())){
				System.out.println("id : " + i + " de : " + message.getEmetteur());
				messagesPersAdmin.add(message);
			}
			i++;
		}
		return messagesPersAdmin;
	}
	
	/**
	 * Methode permettant d'afficher la liste de message de l'etudiant
	 * @param messages
	 * @param etudiant
	 * @return
	 */
	public List<Message> afficherListMessageLire(List<Message> messages, Etudiant etudiant) {
		int i = 0;
		List<Message> messagesEtudiant = new ArrayList<Message>();
		for(Message message : messages) {
			if (message.getDestinataire().getNom().equals(etudiant.getNom())){
				System.out.println("id : " + i + " de : " + message.getEmetteur());
				messagesEtudiant.add(message);
				i++;
			}
		}

		return messagesEtudiant;
	}

	/**
	 * Methode permettant d'afficher la liste de message du professeur
	 * @param messages
	 * @param professeur
	 * @return
	 */
	public List<Message> afficherListMessageLire(List<Message> messages, Professeur professeur) {
		int i = 0;
		List<Message> messagesProfesseur = new ArrayList<Message>();
		for(Message message : messages) {
			if (message.getDestinataire().getNom().equals(professeur.getNom())){
				System.out.println("id : " + i + " de : " + message.getEmetteur());
				messagesProfesseur.add(message);
				i++;
			}
		}
		return messagesProfesseur;
	}

	/**
	 * Methode permettant d'afficher la liste de message du personnel administratif
	 * @param messages
	 * @param personnelAdministratif
	 * @return
	 */
	public List<Message> afficherListMessageLire(List<Message> messages, PersonnelAdministratif personnelAdministratif) {
		int i = 0;
		List<Message> messagesPersAdmin = new ArrayList<Message>();
		for(Message message : messages) {
			if (message.getDestinataire().getNom().equals(personnelAdministratif.getNom())){
				System.out.println("id : " + i + " de : " + message.getEmetteur());
				messagesPersAdmin.add(message);
				i++;
			}
		}
		return messagesPersAdmin;
	}
}
