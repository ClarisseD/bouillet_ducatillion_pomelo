package main.command.model;

/**
 * Importations
 */
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import bdd.Etablissement;
import communication.Message;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;
import users.Utilisateur;

/**
 * Classe Command
 * @author Bouillet Ducatillion
 *
 */
public abstract class Command {

	/**
	 * Attributs
	 */
	private String name;
	private List<Parameter> allowedParameters;
	private String helpMessage;
	
	/**
	 * Getter de name
	 * @return le nom de la commande
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Getter de allowedParameters
	 * @return la liste des parametres autorises
	 */
	public List<Parameter> getAllowedParameters() {
		return allowedParameters;
	}
	
	/**
	 * Methode getHelpMessage pour afficher les differents parametres possible a utiliser pour cette commande
	 * @return les differents parametres pour cette commande
	 */
	public String getHelpMessage(int type) {
	    String commandStr = "  " + name + " ";
		for (Parameter parameter : allowedParameters) {
			commandStr += parameter.toString() + " ";
		}
		commandStr += "\n";
		for (Parameter parameter : allowedParameters) {
			commandStr += "    -" + parameter.getHelpParamater() + "\n";
		}
		return helpMessage + "\n" + commandStr.toString();
	}
	
	/**
	 * Constructeur de Command
	 * @param name
	 * @param helpMessage
	 * @param allowedParameter
	 */
	public Command(String name, String helpMessage, List<Parameter> allowedParameter) {
		this.name = name;
		this.helpMessage = helpMessage;
		this.allowedParameters = allowedParameter;
	}
	
	/**
	 * Surcharge de la methode toString pour obtenir les parametres possibles de cette commande
	 */
	@Override
	public String toString() {
		String commandStr = name + " ";
		for (Parameter parameter : allowedParameters) {
			commandStr += parameter.toString();
		}
		return commandStr.toString();
	}
	
	/**
	 * Methode permettant de convertir une chaine de caractere en un parametre si elle correspond bien a un parametre
	 * @param name
	 * @return le parametre correspond a la chaine de caractere sinon null
	 */
	public Parameter getParameter(String name) {
		for (Parameter parameter : allowedParameters) {
			if (parameter.getShortName().equals(name)) {
				return parameter;
			}
			if (parameter.getName().equals(name)) {
				return parameter;
			}
		}
		return null;
	}
	
	/**
	 * Methodes abstraites correspondant aux executables
	 * @param args
	 * @return
	 * @throws SQLException
	 */
	public abstract Utilisateur execint(String[] args, Etablissement etablissement) throws SQLException;
	public abstract void exec(String[] args,int type, Professeur professeur, Etablissement etablissement, List<Message> messages) throws SQLException;
	public abstract void exec(String[] args,int type, Etudiant etudiant, Etablissement etablissement, List<Message> messages) throws SQLException;
	public abstract void exec(String[] args,int type, PersonnelAdministratif personnelAdministratif, Etablissement etablissement, List<Message> messages) throws SQLException, ParseException, IOException;

}
