package main.command.model;

/**
 * Classe Parameter
 * @author Bouilet Ducatillion
 *
 */
public class Parameter {
	
	/**
	 * Attributs
	 */
	private String name;
	private String shortName;
	private String description;
	private boolean mandatory;
	private boolean noValue;
	
	/**
	 * Getter de name
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Getter de shortName
	 * @return shortName
	 */
	public String getShortName() {
		return shortName;
	}
	
	/**
	 * Getter et setter de mandatory
	 * @return mandatory
	 */
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	
	/**
	 * Constructeur de Parameter (valeur presente)
	 * @param name
	 * @param shortName
	 * @param description
	 * @param mandatory (parametre obligatoire ou non pour la commande)
	 */
	public Parameter(String name, String shortName, String description, boolean mandatory) {
		this.name = name;
		this.shortName = shortName;
		this.description = description;
		// Il y a une valeur pour ce parametre
		this.noValue = false;
		this.setMandatory(mandatory);
	}
	
	/**
	 * Constructeur de Parameter
	 * @param name
	 * @param shortName
	 * @param description
	 * @param mandatory
	 * @param noValue
	 */
	public Parameter(String name, String shortName, String description, boolean mandatory, boolean noValue) {
		this.name = name;
		this.shortName = shortName;
		this.description = description;
		this.noValue = noValue;
		this.setMandatory(mandatory);
	}
	
	/**
	 * Surcharge de la methode toString permettant d'obtenir le nom du parameter
	 */
	@Override
	public String toString() {
		String str = "(--" + name + "|-" + shortName + ")";
		if (noValue) {
			return str;
		}
		return str + "=" + name + " ";
	}
	
	/**
	 * Methode getHelpParameter
	 * @return le nom du parameter
	 */
	public String getHelpParamater() {
		return name + ": " + description;
	}
	
	/**
	 * Methode isPresent permet de verifier si le parameter est present dans la liste prise en entree
	 * @param args
	 * @return true si le parameter est present
	 */
	public boolean isPresent(String[] args) {
		for (String arg : args) {
			String search = "-" + shortName;
			int index = arg.indexOf(search);
			if (index != -1) {
				return true;
			}
			search = "--" + name;
			index = arg.indexOf(search);
			if (index != -1) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Methode permettant de recuperer la valeur du parametre pris en entree
	 * @param args
	 * @return la valeur du parametre
	 */
	public String getValue(String[] args) {
		String search;
		int index;
		for (String arg : args) {
			search = "-" + shortName + "=";
			index = arg.indexOf(search);
			if (index == -1) {
				search = "--" + name + "=";
				index = arg.indexOf(search);
			}
			if (index == -1) {
				continue;
			}
			return arg.substring(search.length(), arg.length());
		}
		return null;
	}
}
