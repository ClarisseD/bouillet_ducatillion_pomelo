package main.command;

/**
 * Importations
 */
import java.util.ArrayList;
import java.util.List;

import main.command.model.Parameter;
import main.command.model.Command;

/**
 * Classe Command
 * @author Bouillet Ducatillion
 *
 */
public class Commands {

	/**
	 * Attribut
	 */
	private static List<Command> commands;

	/**
	 * Methode permettant de recuperer les fonctions existantes
	 * @return la liste de commandes
	 */
	public static List<Command> getRegisterCommands(int type) {
		if (commands != null && type != 0){
			commands = new ArrayList<Command>();
			// help command
			{
				List<Parameter> params = new ArrayList<Parameter>();
				params.add(new Parameter("commandName", "cmd", "Nom de la commande a afficher, affiche l'aide generale sinon", false));
				Command helpCommand = new HelpCommand("help", "Commande d'aide de l'application Pomelo", params);
				commands.add(helpCommand);
			}
			// communication command
			{
				List<Parameter> params = new ArrayList<Parameter>();
				params.add(new Parameter("ecrire", "e", "ecrire un message a quelqu'un", false, true));
				params.add(new Parameter("lire", "l", "lire un message", false, true));
				params.add(new Parameter("supprimer", "s", "supprimer un message", false, true));
				Command communicationCommand = new CommunicationCommand("message", "Onglet communication avec d'autres utilisateurs", params);
				commands.add(communicationCommand);
			}

			// bulletin command
			{
				List<Parameter> params = new ArrayList<Parameter>();
				if (type == 2){
					params.add(new Parameter("visualiser", "v", "visualiser son bulletin", false, true));
				}
				if (type == 1){
					params.add(new Parameter("saisir", "s", "saisir des notes ou des appreciations", false));
				}
				if (type == 3){
					params.add(new Parameter("imprimer", "i", "imprimer un bulletin en particulier", false, true));
				}
				Command bulletinCommand = new BulletinCommand("bulletin", "Onglet bulletin de l'application", params);
				commands.add(bulletinCommand);
			}

			// emploiDuTemps command
			{
				List<Parameter> params = new ArrayList<Parameter>();
				if (type == 1 || type == 2) {
					params.add(new Parameter("visualiser", "v", "visualiser son emploi du temps", false, true)); //je suis pas sur des dernier parametres ^^'
				}
				if (type == 3) {
					params.add(new Parameter("modifier", "m", "ajouter ou supprimer un cours dans un emploi du temps", false)); //idem pour le/les derniers param
				}
				Command emploiDuTempsCommand = new EmploiDuTempsCommand("emploiDuTemps", "Onglet emploi du temps de l'application", params);
				commands.add(emploiDuTempsCommand);
			}

			// ficheInformation Command
			{
				List<Parameter> params = new ArrayList<Parameter>();
				params.add(new Parameter("visualiser", "v", "visualiser sa fiche d'information", false, true));
				Command ficheInformationCommand = new FicheInformationCommand("ficheInformation", "Onglet fiche information de l'application", params);
				commands.add(ficheInformationCommand);
			}

			// gestionSalle Command
			{
				if (type == 3) {
					List<Parameter> params = new ArrayList<Parameter>();
					params.add(new Parameter("modifier", "m", "modifier une salle", false));
					Command gestionSalleCommand = new GestionSalleCommand("gestionSalle", "Onglet gestion salles de l'etablissement", params);
					commands.add(gestionSalleCommand);
				}
			}
		}
		if (commands == null) {
			commands = new ArrayList<Command>();
			//login command
			{
				List<Parameter> params = new ArrayList<Parameter>();
				params.add(new Parameter("user", "u", "username", true));
				params.add(new Parameter("password", "pwd", "mot de passe", true));
				Command loginCommand = new LoginCommand("login", "Connexion au compte utilisateur", params);
				commands.add(loginCommand);
			}
		}
		return commands;
	}

	/**
	 * Methode permettant de trouver la commande dans la liste des commandes possibles
	 * @param commandLine
	 * @param type
	 * @return commande
	 */
	public static Command find(String commandLine, int type) {
		int index = commandLine.indexOf(" ");
		if (index == -1) {
			index = commandLine.length();
		}
		String cmdName = commandLine.substring(0, index);
		for (Command command : getRegisterCommands(type)) {
			if (command.getName().equals(cmdName)) {
				return command;
			}
		}
		return null;
	}

}
