package main.command;

/**
 * Importations
 */
import java.sql.SQLException;
import java.util.List;

import bdd.Etablissement;
import communication.Message;
import main.Main;
import main.command.model.Command;
import main.command.model.Parameter;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;
import users.Utilisateur;

/**
 * Classe CommunicationCommand qui herite de Command
 * @author Bouillet Ducatillion
 *
 */
public class CommunicationCommand extends Command{

	/**
	 * Constructeur de CommunicationCommand
	 * @param name
	 * @param helpMessage
	 * @param allowedParameter
	 */
	public CommunicationCommand(String name, String helpMessage, List<Parameter> allowedParameter) {
		super(name, helpMessage, allowedParameter);
	}

	/**
	 * Surcharge de la methode getHelpMessage
	 */
	@Override
	public String getHelpMessage(int type) {
		return super.getHelpMessage(type)
				+ "message --ecrire || --lire || --supprimer" + "\n";
	}

	/**
	 * Executable pour un professeur
	 */
	@Override
	public void exec(String[] args,int type, Professeur professeur, Etablissement eta, List<Message> messages) throws SQLException {
		boolean cmdToDisplayEcrire = getParameter("ecrire").isPresent(args);
		boolean cmdToDisplayLire = getParameter("lire").isPresent(args);
		boolean cmdToDisplaySupprimer = getParameter("supprimer").isPresent(args);

		if (cmdToDisplayEcrire && cmdToDisplayLire && cmdToDisplaySupprimer) {
			System.out.println("La commande 'message' est mal ecrite" + "\n" + "Vous ne pouvez pas ecrire et lire en meme temps");
			System.out.println(this.toString());
		}

		else if (!cmdToDisplayEcrire && !cmdToDisplayLire && !cmdToDisplaySupprimer) {
			System.out.println("liste des messages : ");
			List<Message> messagesProfesseur = professeur.afficherListMessageLire(messages, professeur);
			if (messagesProfesseur.size() == 0){
				System.out.println("Vous n'avez pas de messages");
			}
		}

		else if (cmdToDisplayEcrire) {
			Message message = professeur.ecrire(eta);
			professeur.ajoutMessageBDD(message);
		}

		else if (cmdToDisplayLire) {
			System.out.println("liste des messages : ");
			List<Message> messagesProfesseur = professeur.afficherListMessageLire(messages, professeur);
			if (messagesProfesseur.size() == 0){
				System.out.println("Vous n'avez pas de messages");
			}
			else{
				System.out.println("Entrez l'identifiant du message que vous voulez lire");
				int id = Main.getScanner().nextInt();
				System.out.println(professeur.lire(messages.get(id)));
			}
		}
		
		else if (cmdToDisplaySupprimer) {
			System.out.println("liste des messages : ");
			List<Message> messagesProfesseur = professeur.afficherListMessage(messages, professeur);
			if (messagesProfesseur.size() == 0){
				System.out.println("Vous n'avez pas de messages");
			}
			else{
				System.out.println("Entrez l'identifiant du message que vous voulez supprimer");
				int id = Main.getScanner().nextInt();
				professeur.supprimerMessageBDD(messages.get(id));
			}
		}
	}

	/**
	 * Executable pour un etudiant
	 */
	@Override
	public void exec(String[] args,int type, Etudiant etudiant, Etablissement eta, List<Message> messages) throws SQLException {
		boolean cmdToDisplayEcrire = getParameter("ecrire").isPresent(args);
		boolean cmdToDisplayLire = getParameter("lire").isPresent(args);
		boolean cmdToDisplaySupprimer = getParameter("supprimer").isPresent(args);
		
		if (cmdToDisplayEcrire && cmdToDisplayLire && cmdToDisplaySupprimer) {
			System.out.println("La commande 'message' est mal ecrite" + "\n" + "Vous ne pouvez pas ecrire et lire en meme temps");
			System.out.println(this.toString());
		}

		else if (!cmdToDisplayEcrire && !cmdToDisplayLire && !cmdToDisplaySupprimer) {
			System.out.println("liste des messages : ");
			List<Message> messagesEtudiant = etudiant.afficherListMessageLire(messages, etudiant);
			if (messagesEtudiant.size() == 0){
				System.out.println("Vous n'avez pas de messages");
			}
		}

		else if (cmdToDisplayEcrire) {
			Message nouveauMessage = etudiant.ecrire(eta);
			etudiant.ajoutMessageBDD(nouveauMessage);; 
		}

		else if (cmdToDisplayLire) {
			System.out.println("liste des messages : ");
			List<Message> messagesEtudiant = etudiant.afficherListMessageLire(messages, etudiant);
			if (messagesEtudiant.size() == 0){
				System.out.println("Vous n'avez pas de messages");
			}
			else{
				System.out.println("Entrez l'identifiant du message que vous voulez lire");
				int id = Main.getScanner().nextInt();
				System.out.println(etudiant.lire(messagesEtudiant.get(id)));
			}
		}
		
		else if (cmdToDisplaySupprimer) {
			System.out.println("liste des messages : ");
			List<Message> messagesProfesseur = etudiant.afficherListMessage(messages, etudiant);
			if (messagesProfesseur.size() == 0){
				System.out.println("Vous n'avez pas de messages");
			}
			else{
				System.out.println("Entrez l'identifiant du message que vous voulez supprimer");
				int id = Main.getScanner().nextInt();
				etudiant.supprimerMessageBDD(messages.get(id));
			}
		}
	}

	/**
	 * Executable pour un personnel administratif
	 */
	@Override
	public void exec(String[] args, int type, PersonnelAdministratif personnelAdministratif, Etablissement eta, List<Message> messages) throws SQLException {
		boolean cmdToDisplayEcrire = getParameter("ecrire").isPresent(args);
		boolean cmdToDisplayLire = getParameter("lire").isPresent(args);
		boolean cmdToDisplaySupprimer = getParameter("supprimer").isPresent(args);
		
		if (cmdToDisplayEcrire && cmdToDisplayLire && cmdToDisplaySupprimer) {
			System.out.println("La commande 'message' est mal ecrite" + "\n" + "Vous ne pouvez pas ecrire et lire en meme temps");
			System.out.println(this.toString());
		}

		if (!cmdToDisplayEcrire && !cmdToDisplayLire && !cmdToDisplaySupprimer) {
			System.out.println("liste des messages : ");
			List<Message> messagesPersAdmin = personnelAdministratif.afficherListMessageLire(messages, personnelAdministratif);
			if (messagesPersAdmin.size() == 0){
				System.out.println("Vous n'avez pas de messages");
			}
		}

		if (cmdToDisplayEcrire) {
			Message message = personnelAdministratif.ecrire(eta);
			personnelAdministratif.ajoutMessageBDD(message);
		}

		if (cmdToDisplayLire) {
			System.out.println("liste des messages : ");
			List<Message> messagesPersAdmin = personnelAdministratif.afficherListMessageLire(messages, personnelAdministratif);
			if (messagesPersAdmin.size() == 0){
				System.out.println("Vous n'avez pas de messages");
			}
			else {
				System.out.println("Entrez l'identifiant du message que vous voulez lire");
				int id = Main.getScanner().nextInt();
				System.out.println(personnelAdministratif.lire(messages.get(id)));
			}
		}
		
		else if (cmdToDisplaySupprimer) {
			System.out.println("liste des messages : ");
			List<Message> messagesProfesseur = personnelAdministratif.afficherListMessage(messages, personnelAdministratif);
			if (messagesProfesseur.size() == 0){
				System.out.println("Vous n'avez pas de messages");
			}
			else{
				System.out.println("Entrez l'identifiant du message que vous voulez supprimer");
				int id = Main.getScanner().nextInt();
				personnelAdministratif.supprimerMessageBDD(messages.get(id));
			}
		}
	}

	/**
	 * Executable pour un utilisateur
	 */
	@Override
	public Utilisateur execint(String[] args, Etablissement eta) throws SQLException {
		return null;
	}

}
