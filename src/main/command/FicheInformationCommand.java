package main.command;

/**
 * Importations
 */
import java.sql.SQLException;
import java.util.List;

import bdd.Etablissement;
import communication.Message;
import main.command.model.Command;
import main.command.model.Parameter;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;
import users.Utilisateur;

/**
 * Classe FicheInforationCommand qui herite de Command
 * @author Bouillet Ducatillion
 *
 */
public class FicheInformationCommand extends Command{

	/**
	 * Constructeur de FicheInformationCommand
	 * @param name
	 * @param helpMessage
	 * @param allowedParameter
	 */
	public FicheInformationCommand(String name, String helpMessage, List<Parameter> allowedParameter) {
		super(name, helpMessage, allowedParameter);
	}

	/**
	 * Surcharge de la methode getHelpMessage
	 */
	@Override
	public String getHelpMessage(int type) {
			return super.getHelpMessage(type)
					+ "ficheInformation --visualiser" + "\n";
	}

	/**
	 * Executable pour un etudiant
	 */
	@Override
	public void exec(String[] args,int type, Etudiant etudiant, Etablissement etablissement, List<Message> messages) throws SQLException {
		boolean cmdToDisplayVisualiser = getParameter("visualiser").isPresent(args);

		if (cmdToDisplayVisualiser) {
			String ficheInformation = etudiant.visualiserFiche(etudiant); 
			System.out.println(ficheInformation);
		}
	}

	/**
	 * Executable pour un professeur
	 */
	@Override
	public void exec(String[] args,int type, Professeur professeur, Etablissement eta, List<Message> messages) throws SQLException {
		boolean cmdToDisplayVisualiser = getParameter("visualiser").isPresent(args);

		if (cmdToDisplayVisualiser) {
			String ficheInformation = professeur.visualiserFiche(professeur); 
			System.out.println(ficheInformation);
		}
	}

	/**
	 * Executable pour un personnel administratif
	 */
	@Override
	public void exec(String[] args,int type, PersonnelAdministratif personnelAdministratif, Etablissement etablissement, List<Message> messages) throws SQLException {
		boolean cmdToDisplayVisualiser = getParameter("visualiser").isPresent(args);

		if (cmdToDisplayVisualiser) {
			String ficheInformation = personnelAdministratif.visualiserFiche(personnelAdministratif); 
			System.out.println(ficheInformation);
		}
	}

	/**
	 * Executable pour un utilisateur
	 */
	@Override
	public Utilisateur execint(String[] args, Etablissement eta) throws SQLException {
		return null;
	}

}
