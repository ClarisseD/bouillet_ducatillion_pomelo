package main.command;

/**
 * Importations
 */
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import bdd.Etablissement;
import communication.Message;
import emploiDuTemps.Cours;
import emploiDuTemps.CreationEmploiDuTemps;
import emploiDuTemps.EmploiDuTemps;
import main.command.model.Command;
import main.command.model.Parameter;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;
import users.Utilisateur;

/**
 * Classe EmploiDuTempsCommand qui herite de Command
 * @author Bouillet Ducatillion
 *
 */
public class EmploiDuTempsCommand extends Command {

	/**
	 * Constructeur EmploiDuTempsCommand
	 * @param name
	 * @param helpMessage
	 * @param allowedParameter
	 */
	public EmploiDuTempsCommand(String name, String helpMessage, List<Parameter> allowedParameter) {
		super(name, helpMessage, allowedParameter);
	}

	/**
	 * Methode pour afficher les messages d'aide
	 */
	@Override
	public String getHelpMessage(int type) {
		if (type == 1 || type == 2){
			return super.getHelpMessage(type)
					+ "emploiDuTemps --visualiser" + "\n";
		}
		else{
			return super.getHelpMessage(type)
				+ "emploiDuTemps --modifier=ajout || --modifier=suppression || --modifier=modification" + "\n";
		}
	}

	/**
	 * Executable pour un professeur
	 */
	@Override
	public void exec(String[] args,int type, Professeur professeur, Etablissement eta, List<Message> messages) throws SQLException {
		boolean cmdToDisplayVisualiser = getParameter("visualiser").isPresent(args);
		
		EmploiDuTemps emploiDuTemps = CreationEmploiDuTemps.creationEmploiDuTempsProfesseur(professeur.getNom(), eta);
		
		if (cmdToDisplayVisualiser) {
			professeur.visualiserEdT(emploiDuTemps);
		}
		else {
			System.out.println("La commande 'visualiser' est mal ecrite");
		}
	}

	/**
	 * Executable pour un etudiant
	 */
	@Override
	public void exec(String[] args,int type, Etudiant etudiant, Etablissement etablissement, List<Message> messages) throws SQLException {
		boolean cmdToDisplayVisualiser = getParameter("visualiser").isPresent(args);
		
		EmploiDuTemps emploiDuTemps = CreationEmploiDuTemps.creationEmploiDuTemps(etudiant.getPromotionString(), etablissement);
		
		if (cmdToDisplayVisualiser) {
			etudiant.visualiserEdT(emploiDuTemps);
		}
		else {
			System.out.println("La commande 'visualiser' est mal ecrite");
		}
	}

	/**
	 * Executable pour un personnel administratif
	 */
	@Override
	public void exec(String[] args,int type, PersonnelAdministratif personnelAdministratif, Etablissement eta, List<Message> messages) throws SQLException, ParseException {
		String modifier = getParameter("modifier").getValue(args);
		
		if(modifier.equals("ajout")) {
			System.out.println("Veuillez suivre les instructions pour creer le cours a ajouter");
			Cours cours = personnelAdministratif.saisieCours(eta);
			personnelAdministratif.ajoutCoursEDT(cours);
		}
		
		else if(modifier.equals("suppression")) {
			System.out.println("Veuillez suivre les instructions pour creer le cours a supprimer");
			Cours cours = personnelAdministratif.saisieCours(eta);
			personnelAdministratif.suppressionCoursEDT(cours);
		}
		
		else if(modifier.equals("modification")) {
			EmploiDuTemps emploiDuTemps = personnelAdministratif.choixPromotionCreation(eta);
			personnelAdministratif.majEDT(eta, emploiDuTemps);
		}
		
		else {
			System.out.println("La commande 'modifier' est mal ecrite");
		}
	}
	
	/**
	 * Executable pour un utilisateur
	 */
	@Override
	public Utilisateur execint(String[] args, Etablissement eta) throws SQLException {
		return null;
	}

}
