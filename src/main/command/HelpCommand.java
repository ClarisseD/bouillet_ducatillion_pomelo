package main.command;

/**
 * Importations
 */
import java.sql.SQLException;
import java.util.List;

import bdd.Etablissement;
import communication.Message;
import main.command.model.Command;
import main.command.model.Parameter;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;
import users.Utilisateur;

/**
 * Classe HelpCommand qui herite de Command
 * @author Bouillet Ducatillion
 *
 */
public class HelpCommand extends Command {

	/**
	 * Constructeur de HelpCommand
	 * @param name
	 * @param helpMessage
	 * @param allowedParameter
	 */
	public HelpCommand(String name, String helpMessage, List<Parameter> allowedParameter) {
		super(name, helpMessage, allowedParameter);
	}
	
	/**
	 * Executable pour un professeur
	 */
	@Override
	public void exec(String[] args,int type, Professeur professeur, Etablissement eta, List<Message> messages) {
		if (args.length < 2) {
			System.out.println(this.getHelpMessage(type));
			return;
		}

		String cmdToDisplay = getParameter("cmd").getValue(args);
		if (cmdToDisplay == null) {
			System.out.println("La commande 'help' est mal ecrite");
			System.out.println(this.toString());
			return;
		}
		
		Command command = Commands.find(cmdToDisplay, type);
		if (command != null) {
			System.out.println(command.getHelpMessage(type));
		} else {
			System.out.println("Commande '" + cmdToDisplay + "' inconnue");
		}
	}
	
	/**
	 * Executable pour un etudiant
	 */
	@Override
	public void exec(String[] args,int type, Etudiant etudiant, Etablissement etablissement, List<Message> messages) throws SQLException {
		if (args.length < 2) {
			System.out.println(this.getHelpMessage(type));
			return;
		}

		String cmdToDisplay = getParameter("cmd").getValue(args);
		if (cmdToDisplay == null) {
			System.out.println("La commande 'help' est mal ecrite");
			System.out.println(this.toString());
			return;
		}
		
		Command command = Commands.find(cmdToDisplay, type);
		if (command != null) {
			System.out.println(command.getHelpMessage(type));
		} else {
			System.out.println("Commande '" + cmdToDisplay + "' inconnue");
		}
	}

	/**
	 * Executable pour un personnel administratif
	 */
	@Override
	public void exec(String[] args,int type, PersonnelAdministratif personnelAdministratif, Etablissement etablissement, List<Message> messages) throws SQLException {
		if (args.length < 2) {
			System.out.println(this.getHelpMessage(type));
			return;
		}

		String cmdToDisplay = getParameter("cmd").getValue(args);
		if (cmdToDisplay == null) {
			System.out.println("La commande 'help' est mal ecrite");
			System.out.println(this.toString());
			return;
		}
		
		Command command = Commands.find(cmdToDisplay, type);
		if (command != null) {
			System.out.println(command.getHelpMessage(type));
		} else {
			System.out.println("Commande '" + cmdToDisplay + "' inconnue");
		}
	}

	/**
	 * Executable pour un utilisateur
	 */
	@Override
	public Utilisateur execint(String[] args, Etablissement eta) throws SQLException {
		return null;
	}
}
