package main;

/**
 * Importations
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe SingletonConnection
 * @author Bouillet Ducatillion
 *
 */
public class SingletonConnection {

	private final String URL = "jdbc:postgresql://localhost:5432/etablissement";
	private final String USER = "postgres";
	private final String PASS = "postgres";
	
	private static Connection connection;
	
	/**
	 * Constructeur SingletonConnection
	 */
	private SingletonConnection() {
		try{
			Class.forName("org.postgresql.Driver"); 
			connection = DriverManager.getConnection(URL, USER, PASS); 
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Imposible de creer la connexion");
		} catch(ClassNotFoundException e){
			System.err.println("Le driver n'est pas installe"); 
		}
	}
	
	/**
	 * Methode permettant de se connecter a la base de donnees
	 * @return connection a la bdd
	 */
	public static Connection getInstance() {
		if (connection == null) {
			new SingletonConnection();
		}
		return connection;
	}
	
}
