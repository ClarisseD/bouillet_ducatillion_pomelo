package main;

/**
 * Importation
 */
import users.Utilisateur;

/**
 * Classe Pomelo
 * @author Bouillet Ducatillion
 *
 */
public class Pomelo {
	
	/**
	 * Attributs
	 */
	Utilisateur utilisateurConnecte; 
	private static Pomelo instance;
	
	/**
	 * Constructeur de Pomelo
	 */
	public Pomelo() {
	}
	
	/**
	 * Getter d'instance
	 * @return instances
	 */
	public static Pomelo getInstance() {
		if (instance == null) {
			instance = new Pomelo();
		}
		return instance;
	}
	
}
