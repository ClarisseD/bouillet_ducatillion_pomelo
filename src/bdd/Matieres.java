package bdd;

/**
 * Importations
 */
import java.util.ArrayList;
import java.util.List;

/**
 * Classe Matieres
 * @author Bouillet Ducatillion
 *
 */
public class Matieres {

	/**
	 * Attribut
	 */
	private static List<Matiere> matieres;

	/**
	 * Getter de matieres et permet de la construire s'il y faut
	 * @return la liste des matieres de l'etablissement
	 */
	public static List<Matiere> getMatieres() {
		if (matieres == null) {
			matieres = new ArrayList<Matiere>();
			matieres.add(Matiere.Topometrie);
			matieres.add(Matiere.Photogrammetrie);
			matieres.add(Matiere.Informatique);
			matieres.add(Matiere.Teledetection);
			matieres.add(Matiere.Mathematiques);
			matieres.add(Matiere.Anglais);
			matieres.add(Matiere.LV2);
			matieres.add(Matiere.Geodesie);
		}
		return matieres;
	}

	/**
	 * Methode premettant de convertir une chaine de caractere en la matiere correspondante
	 * @param nom
	 * @return la matiere correspondante au nom, si le nom ne correspond a aucune matiere, retourn null
	 */
	public static Matiere get(String nom) {
		for (Matiere matiere : getMatieres()) {
			nom = nom.replaceAll("\\s", "");
			if (matiere.toString().equals(nom)) {
				return matiere;
			}
		}
		return null;
	}

}
