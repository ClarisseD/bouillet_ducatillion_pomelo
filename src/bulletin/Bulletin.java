package bulletin;

/**
 * Importations
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bdd.Matiere;
import bdd.Matieres;
import main.Main;
import users.Etudiant;

/**
 * Classe Bulletin
 * @author Bouillet Ducatillion
 *
 */
public class Bulletin {
	
	/**
	 * Attributs
	 */
	private String nom;
	private String prenom;
	private List<List<Note>> notes;
	private List<Appreciation> appreciations;
	
	/**
	 * Getter et setter du nom de l'etudiant
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Getter et setter du prenom de l'etudiant
	 * @return
	 */
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	/**
	 * Getter de notes
	 * @return la liste des notes de l'etudiant
	 */
	public List<List<Note>> getNotes() {
		return notes;
	}
	
	/**
	 * Getter de appreciations
	 * @return la liste des appreciations concernant l'etudiant
	 */
	public List<Appreciation> getAppreciations() {
		return appreciations;
	}

	
	/**
	 * Constructeur Bulletin
	 * @param etudiant
	 */
	public Bulletin(String nom, String prenom) {
		this.setNom(nom);
		this.setPrenom(prenom);
		this.notes = new ArrayList<List<Note>>();
		this.appreciations = new ArrayList<Appreciation>();
		
		for (int i=0; i<8; i++) {
			List<Note> lNotes = new ArrayList<Note>();
			lNotes.add(new Note(Matieres.getMatieres().get(i), 0, 0));
			notes.add(lNotes);
		}
	}
	
	/**
	 * Methode permet de creer un bulletin
	 * @param etudiant
	 * @return etudiant avec son bulletin modifie
	 */
	public static Etudiant creationBulletin(Etudiant etudiant) {
		List<Note> listNotes = new ArrayList<Note>();
		Note note= null;
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM notes WHERE nometudiant = ? AND prenometudiant = ?");
			statement.setString(1, etudiant.getNom());
			statement.setString(2, etudiant.getPrenom());
			ResultSet resultat = statement.executeQuery();
			while(resultat.next()) {
				int coeff = resultat.getInt(3);
				String matiereStr = resultat.getString(4);
				Matiere matiere = Matieres.get(matiereStr);
				float valeur = resultat.getFloat(5);
				note = new Note(matiere, coeff, valeur);
				listNotes.add(note);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		Appreciation appreciation = null;
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM appreciation WHERE nom = ? AND prenom = ?");
			statement.setString(1, etudiant.getNom());
			statement.setString(2, etudiant.getPrenom());
			ResultSet resultat = statement.executeQuery();
			if(resultat.next()) {
				String matiereStr = resultat.getString(3);
				Matiere matiere = Matieres.get(matiereStr);
				String contenu = resultat.getString(4);
				appreciation = new Appreciation(matiere, contenu);
				etudiant.getBulletin().getAppreciations().add(appreciation);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		for (Note notes : listNotes) {
			int i = 0;
			while(i<8) {
				if(etudiant.getBulletin().getNotes().get(i).get(0).getMatiere() == notes.getMatiere()) {
					etudiant.getBulletin().getNotes().get(i).add(notes);
					break;
				}
				i++;
			}

		}
		return etudiant;
	}
}
