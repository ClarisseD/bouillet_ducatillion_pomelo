package bulletin;

/**
 * Importation
 */
import bdd.Matiere;

/**
 * Classe Appreciation
 * @author Bouillet Ducatillion
 *
 */
public class Appreciation {
	
	/**
	 * Attributs
	 */
	private Matiere matiere;
	private String contenu;
		
	/**
	 * Constructeur de Appreciation
	 * @param matiere
	 * @param contenu
	 */
	public Appreciation(Matiere matiere, String contenu) {
		this.matiere = matiere;
		this.contenu = contenu;
	}
	
	/**
	 * Getter de matiere
	 * @return la matiere concernee par l'appreciation
	 */
	public Matiere getMatiere() {
		return matiere;
	}

	/**
	 * Getter de contenu
	 * @return le contenu de l'appreciation
	 */
	public String getContenu() {
		return contenu;
	}
}
