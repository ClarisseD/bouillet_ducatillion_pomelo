package bulletin;

/**
 * Importation
 */
import bdd.Matiere;

/**
 * Classe Note
 * @author Bouillet Ducatillion
 *
 */
public class Note {
	
	/**
	 * Attributs
	 */
	private Matiere matiere;
	private int coeff;
	private float valeur;
	
	/**
	 * Getter et setter de matiere
	 * @return la matiere concernee par la note
	 */
	public Matiere getMatiere() {
		return matiere;
	}
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}
	
	/**
	 * Getter et setter de coeff
	 * @return le coeff de la note
	 */
	public int getCoeff() {
		return coeff;
	}
	public void setCoeff(int coeff) {
		this.coeff = coeff;
	}
	
	/**
	 * Getter et setter valeur
	 * @return la valeur de la note
	 */
	public float getValeur() {
		return valeur;
	}
	public void setValeur(float valeur) {
		this.valeur = valeur;
	}

	/**
	 * Constructeur Note
	 * @param matiere
	 * @param coeff
	 * @param valeur
	 */
	public Note(Matiere matiere, int coeff, float valeur) {
		this.setMatiere(matiere);
		this.setCoeff(coeff);
		this.setValeur(valeur);
	}
	
	/**
	 * Constructeur de Note sans la matiere
	 * @param coeff
	 * @param valeur
	 */
	public Note(int coeff, float valeur) {
		this.setCoeff(coeff);
		this.setValeur(valeur);
	}
}
