package communication;

/**
 * Importations
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bdd.Etablissement;
import main.Main;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;
import users.Utilisateur;

/**
 * Classe ListMessage
 * @author Bouillet Ducatillion
 *
 */
public class ListMessage {
	
	/**
	 * Methode permettant de creer la liste des messages 
	 * @param etablissment cree precedemment
	 * @return la liste des messages de la bdd
	 */
	public static List<Message> creationListMessagesBDD(Etablissement etablissment){
		
		List<Message> messages = new ArrayList<Message>();
		
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM messages");
			ResultSet resultat = statement.executeQuery();
			while (resultat.next()){
				String emetteur = resultat.getString(1);
				emetteur = emetteur.trim();
				String destinataireString = resultat.getString(2);
				destinataireString = destinataireString.trim();
				Utilisateur destinataire = null;
				for (Etudiant destinataireR : etablissment.getEtudiants()){
					if (destinataireR.getNom().equals(destinataireString)){
						destinataire = (Utilisateur)destinataireR;
						break;
					}
				}
				for (Professeur destinataireR : etablissment.getProfesseurs()){
					if (destinataireR.getNom().equals(destinataireString)){
						destinataire = (Utilisateur)destinataireR;
						break;
					}
				}
				for (PersonnelAdministratif destinataireR : etablissment.getPersonnelAdministratif()){
					if (destinataireR.getNom().equals(destinataireString)){
						destinataire = (Utilisateur)destinataireR;
						break;
					}
				}				
				String contenu = resultat.getString(3);
				contenu = contenu.trim();
				
				Message message = new Message(emetteur, destinataire, contenu);
				
				messages.add(message);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return messages;
	}
}
