package communication;

/**
 * Importation
 */
import bdd.Etablissement;

/**
 * Interface Communiquable
 * @author Bouillet Ducatillion
 *
 */
public interface Communiquable {
	
	/**
	 * Methode abstraite pour lire un message
	 * @param message
	 * @return 
	 */
	public abstract String lire(Message message);
	
	/**
	 * Methode abstraite pour ecrire un message
	 * @param utilisateur
	 * @return
	 */
	public abstract Message ecrire(Etablissement etablissement);
	
}
