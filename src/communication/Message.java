package communication;


/**
 * Importation
 */
import users.Utilisateur;

/**
 * Classe Message
 * @author Bouillet Ducatillion
 *
 */
public class Message {
	
	private String emetteur;
	private Utilisateur destinataire;
	private String contenu;
	
	/**
	 * Getter emetteur
	 * @return l'emetteur du message
	 */
	public String getEmetteur() {
		return emetteur;
	}
	
	/**
	 * Getter destinataire
	 * @return le destinataire du message
	 */
	public Utilisateur getDestinataire() {
		return destinataire;
	}
	
	/**
	 * Getter contenu
	 * @return le contenu du message
	 */
	public String getContenu() {
		return contenu;
	}
	
	/**
	 * Constructeur de message
	 * @param emetteur
	 * @param destinataire
	 * @param contenu
	 */
	public Message(String emetteur, Utilisateur destinataire, String contenu) {
		this.emetteur = emetteur;
		this.destinataire = destinataire;
		this.contenu = contenu;
	}
	
	
}
