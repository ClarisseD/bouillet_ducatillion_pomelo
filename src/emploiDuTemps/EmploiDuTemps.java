package emploiDuTemps;

/**
 * Importations
 */
import java.util.ArrayList;
import java.util.List;

/**
 * Classe EmploiDuTemps
 * @author Bouillet Ducatillion
 *
 */
public class EmploiDuTemps {
	
	/**
	 * Attribut
	 */
	public List<Cours> edt;
	
	/**
	 * Getter edt
	 * @return la liste de cours composant l'emploi du temps
	 */
	public List<Cours> getEdt(){
		return this.edt;
	}
	
	/**
	 * Constructeur EmploiDuTemps
	 */
	public EmploiDuTemps() {
		this.edt = new ArrayList<Cours>();
	}
}
