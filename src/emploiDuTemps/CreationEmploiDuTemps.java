package emploiDuTemps;

/**
 * Importations
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bdd.Etablissement;
import bdd.Matiere;
import bdd.Matieres;
import main.Main;
import users.Professeur;

/**
 * Classe CreationEmploiDuTemps
 * @author Bouillet Ducatillion
 *
 */
public class CreationEmploiDuTemps {

	/**
	 * Methode permettant de creer l'emploi du temps d'une promotion
	 * @param promotionString
	 * @param eta etablissement cree avant 
	 * @return emploi du temps de la promotion
	 */
	public static EmploiDuTemps creationEmploiDuTemps(String promotionString, Etablissement eta){
		
		EmploiDuTemps edt = new EmploiDuTemps();
		
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM emploidutemps WHERE promotion = ? ORDER BY date, horairedebut");
			statement.setString(1, promotionString);
			ResultSet resultat = statement.executeQuery();
			while (resultat.next()){
				String matiereString = resultat.getString(1);
				Matiere matiere = Matieres.get(matiereString);
				String nom = resultat.getString(2);
				nom = nom.trim();
				Professeur prof = null;
				for (Professeur profR : eta.getProfesseurs()){
					if (profR.getNom().equals(nom)){
						prof = profR;
						break;
					}
				}
				int salleInt = resultat.getInt(3);
				Salle salle = null;
				for (Salle salleR : eta.getSalles()){
					if (salleR.getNumeroSalle() == salleInt){
						salle = salleR;
						break;
					}
				}
				String horaireDebut = resultat.getString(6);
				horaireDebut = horaireDebut.trim();
				String horaireFin = resultat.getString(7);
				horaireFin = horaireFin.trim();
				String date = resultat.getString(5);
				Promotion promotion = Promotion.get(promotionString, eta);
				Cours cours = new Cours(matiere, prof, promotion, salle, horaireDebut, horaireFin, date);
				edt.getEdt().add(cours);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return edt;
	}

	/**
	 * Methode permettant de creer l'emploi du temps du professeur
	 * @param nomProf
	 * @param eta etablissement cree avant 
	 * @return emploi du temps du professeur
	 */
	public static EmploiDuTemps creationEmploiDuTempsProfesseur(String nomProf, Etablissement eta) {
		
		EmploiDuTemps edt = new EmploiDuTemps();

		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM emploidutemps WHERE professeur = ? ORDER BY date, horairedebut");
			statement.setString(1, nomProf);
			ResultSet resultat = statement.executeQuery();
			while (resultat.next()){
				String matiereString = resultat.getString(1);
				Matiere matiere = Matieres.get(matiereString);
				String nom = resultat.getString(2);
				nom = nom.trim();
				Professeur prof = null;
				for (Professeur profR : eta.getProfesseurs()){
					if (profR.getNom().equals(nom)){
						prof = profR;
						break;
					}
				}
				int salleInt = resultat.getInt(3);
				Salle salle = null;
				for (Salle salleR : eta.getSalles()){
					if (salleR.getNumeroSalle() == salleInt){
						salle = salleR;
						break;
					}
				}
				String promotionString = resultat.getString(4);
				Promotion promotion = Promotion.get(promotionString, eta);
				String horaireDebut = resultat.getString(6);
				horaireDebut = horaireDebut.trim();
				String horaireFin = resultat.getString(7);
				horaireFin = horaireFin.trim();
				String date = resultat.getString(5);
				Cours cours = new Cours(matiere, prof, promotion, salle, horaireDebut, horaireFin, date);
				edt.getEdt().add(cours);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		
		return edt;

	}
}
