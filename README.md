# Pomelo

Application de SuperPlanning pour une école

## Authors

* **Bouillet Marie-Laure**
* **Ducatillion Clarisse**

## Installation

* Créer le projet sur votre machine
```
* Créez le projet java vide
* Dans le répertoire du projet, l'ouvrir dans un terminal
    * git init
    * git remote add origin https://gitlab.com/ClarisseD/bouillet_ducatillion_pomelo.git
    * git pull origin master
```

* Restaurer la base de donnée

Veuillez restaurer la base de données à l'aide du fichier **BaseDeDonneeEtablissement.backup**

```
Avec PgAdmin :
* Ouvrez l'application
* Connectez-vous au serveur localhost
* Ajoutez une base de données (new database) de nom: etablissement
* Clic droit sur la base 
* Restaurer (restore) avec le fichier BaseDeDonneeEtablissement.backup
```

* Installer le driver

Veuillez installer le driver **postgresql-42.2.5.jar**

```
Sur Eclipse : 
* Ouvrir le projet récupéré précedemment 
(File, New, Java Project, Use default localisation (décoché), choisir la localisation du projet, Finish)
* Faire un clic droit sur le projet
* Aller dans Properties 
* Aller dans Java Build Path
* Aller dans Libraries
* Cliquer sur Add External JARs...
* Sélectionner postgresql-42.2.5.jar
* Cliquer sur Apply and Close
```

* Lancer l'application 

Lancez l'application en executant le main

## Comment utiliser l'application

### Connection

Veuillez vous connecter avec un couple (nom utilisateur, mot de passe) présents dans la base de donnée (table identifiants).

```
Exemples de commande connection :
* Connection d'un étudiant : login -u=CDucatillion -pwd=DClarisse2 ou login --user=CDucatillion --password=DClarisse2
* Connection d'un professeur : login -u=CBouche -pwd=BClement3
* Connection d'un personnel administratif : login -u=MEvian -pwd=EMarie2
```

### Commandes réalisables

Les commandes que vous pouvez réaliser sont indiquées dans le message de bienvenue.

#### Commande help

La commande help permet de connaître la bonne utilisation des autres commandes de l'application.
Il est fortement conseillé quand vous utilisez une nouvelle commande d'appeler l'aide de celle ci. 

```
Exemple de demande d'aide :
help -cmd=message ou help --commandName=message
```

#### Commande message

La commande message permet de consulter la liste des messages reçus, lire un message en particulier et écrire un message à un destinataire.

```
Commande à entrer pour avoir : 
* Liste des messages reçus : message 
* Lire un message en particulier : message -l
* Ecrire un message à quelqu'un : message -e

Une fois une de ces commandes entrée (une seule est réalisable à la fois), plusieurs indications sont données et doivent être suivies.
```

#### Commande ficheInformation

La commande ficheInformation permet de visualiser sa fiche personnelle.

```
Commande à entrer pour visualiser sa fiche : ficheInformation -v
```

#### Commande gestionSalle

Cette commande est réservée au personnel administratif. Elle permet de gérer la capacité, le type, l'état et les équipements des salles de l'établissement.

```
Commande à entrer pour :
* Gérer la capacité d'une salle : gestionSalle -m=capacite
* Gérer le type d'une salle : gestionSalle -m=type
* Gérer l'état d'une salle : gestionSalle -m=etat
* Gérer les équipements d'une salle : gestionSalle -m=equipement

Une fois une de ces commandes entrée (une seule est réalisable à la fois), plusieurs indications sont données et doivent être suivies.
```

#### Commande emploiDuTemps

* Etudiant et Professeur

L'étudiant et le professeur peuvent visualiser leur emploi du temps avec cette commande.

```
Commande à entrer pour visualiser son emploi du temps : emploiDuTemps -v
```

* Personnel administratif

Le personnel administratif peut ajouter ou supprimer un cours de l'emploi du temps.

```
Commande à entrer pour : 
* Ajouter un cours : emploiDuTemps -m=ajout
* Supprimer un cours : emploiDuTemps -m=suppression

Une fois une de ces commandes entrée (une seule est réalisable à la fois), plusieurs indications sont données et doivent être suivies.
```

#### Commande bulletin

* Etudiant 

L'étudiant peut visualiser son bulletin.

```
Commande à entrer pour visualiser le bulletin : bulletin -v
```

* Professeur

Le professeur peut ajouter des notes et des appréciations pour le bulletin d'un étudiant.

```
Commande à entrer pour : 
* Ajouter une note : bulletin -s=note
* Ajouter une appréciation : bulletin -s=appreciation

Une fois une de ces commandes entrée (une seule est réalisable à la fois), plusieurs indications sont données et doivent être suivies.
```

* PersonnelAdministratif

Le personnel administratif peut imprimer les bulletins.

```
Commande à entrer pour imprimer les bulletins : bulletin -i

La dernière instruction à suivre est : Veuillez entrer le chemin du dossier où sera enregistrer le bulletin
Choisissez une chemin facile d'accès pour consulter les bulletins (format .txt)
```

### Déconnection

Pour se déconnecter de l'application il suffit d'écrire "deconnection" lors d'une demande de commande.